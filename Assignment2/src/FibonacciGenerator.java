import java.util.Scanner;

public class FibonacciGenerator {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int opt = 0;
		try {		
		System.out.println("Menu:-"
				+ "\nSelect one of the options below:-"
				+ "\n 1.Validate a number"
				+ "\n 2.Calculate the nth value in the sequence"
				+ "\n 3.Exit"
				+ "\nProvide with an input: ");
		opt = sc.nextInt();
		}
		catch(Exception e){
			System.out.println("Please enter a correct input");
			main(args);
		}
		switch (opt) {
		case 1:
			val();
			break;
		case 2:
			calc();
			break;
		case 3:
			break;
		default:
			System.out.println("Please enter a correct input");
			main(args);
			break;
		}
	}

	private static void calc() {
		// TODO Auto-generated method stub
		try {	int num;
			Scanner sc = new Scanner(System.in);
			System.out.println("Enter a number to calculate the nth value in the sequence:");
			num=sc.nextInt();
			if(num==1||num==2) {
				System.out.println("Its value in the Fibonacci sequence is 1.");
			}
			else {
				num=fib(num);
				System.out.println("Its value in the Fibonacci sequence is "+ num);
			}
		}
		catch(Exception e){
			System.out.println("Please enter a correct input");
			calc();
		}
	}

	private static int fib(int num) {
		// TODO Auto-generated method stub
		int i,k=1,j=1,l=0;
		for(i=2;i<num;i++) {
			l=k+j;
			k=j;
			j=l;
		}
		return l;
	}

	private static void val() {
		try{ int num;
		Scanner sc = new Scanner(System.in);
		// TODO Auto-generated method stub
		System.out.println("Enter a number to Validate:");
		num=sc.nextInt();
		if(num==1) {
			System.out.println("It exists in the sequence at position 1 and 2.");
		}
		else {
			num=valfib(num);
			if(num==0) {
				System.out.println("Value does not exist in the Fibonacci Sequence.");
			}
			else {
				System.out.println("It exists in the sequence at position "+ num);
			}
		}
		}
		catch(Exception e){
			System.out.println("Please enter a correct input");
			val();
		}
}

	private static int valfib(int num) {
		// TODO Auto-generated method stub
		int i=2,j=1,k=1,l=0;
		while(l<num) {
			i++;
			l=k+j;
			k=j;
			j=l;
			if(l==num) {
				return i;
			}
			else if(l>num) {
				return 0;
			}
		}
		return 0;
	}
}
